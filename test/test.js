const { checkEmail, checkAge } = require("../src/util.js");
const { assert, expect } = require("chai");

describe("test_checkEmail", () => {
  it("email_not_empty", () => {
    const email = "";
    assert.isNotEmpty(checkEmail(email)); 
  });

  it("email_not_undefined", () => {
    const email = undefined;
    // assert.isDefined(checkEmail(email));
    // expect(checkEmail(email)).to.not.be.undefined;
    expect(checkEmail(email)).to.not.equal(undefined);
  });

  it("email_not_null", () => {
    const email = null;
    // assert.isNotNull(checkEmail(email));
    expect(checkEmail(email)).to.not.be.null;
  });

  it("email_is_string_value", () => {
    const email = 5;
    assert.isString(checkEmail(email));
  });
});

describe("test_checkAge", () => {
  it("age_not_empty", () => {
    const age = "";
    assert.isNotEmpty(checkAge(age));
  });

  it("age_is_integer", () => {
    const age = 12;
    // assert.isNumber(age, checkAge(age));
    // assert.strictEqual(typeof checkAge(age), 'number', "Error: Age not a number");
    expect(checkAge(age)).to.be.a("number");
  });

  it("age_not_undefined", () => {
    const age = undefined;
    assert.isDefined(checkAge(age));
  });

  it("age_not_null", () => {
    const age = null;
    assert.isNotNull(checkAge(age));
  });

  it("age_not_less_than_0", () => {
    const age = 1;
    // assert.operator(checkAge(age), '>', 0, 'Error: Age must be greater than 0')
    assert.isAtLeast(checkAge(age), 0, "Error: Age must be greater than 0");
  });
});
