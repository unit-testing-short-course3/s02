function checkEmail(email) {
  if (email == "") {
    return "Error: Email is Empty!";
  }

  if (email === undefined) {
    return "Error: Email should be defined!";
  }

  if (email === null) {
    return "Error: Email should not be null!";
  }

  if (typeof email !== "string") {
    return "Error: Email should be a string value!";
  }

  return email;
}

function checkAge(age) {
  if (age == "") {
    return "InputError: Age must have a value!";
  }

  if (typeof age != "number") {
    return "InputError: Age must be a number!";
  }

  if (age === undefined) {
    return "InputError: Age must be defined!";
  }

  if (age < 0) {
    return "InputError: Age must be greater than 0";
  }

  return age;
}

function checkFullName(fullName) {
  return fullName;
}

module.exports = {
  checkEmail: checkEmail,
  checkAge: checkAge,
  checkFullName: checkFullName,
};
